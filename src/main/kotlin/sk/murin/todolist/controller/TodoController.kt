package sk.murin.todolist.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import sk.murin.todolist.model.Todo
import sk.murin.todolist.model.TodoEditDto
import sk.murin.todolist.service.TodoService

@RestController
@RequestMapping("/todos")
class TodoController(val service: TodoService) {
    private val log: Logger = LoggerFactory.getLogger(TodoService::class.java)

    @PostMapping
    fun addTodo(@RequestBody todo: Todo) = service.addTodo(todo)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Long) = service.deleteById(id).also {
        log.info("simulated delete request deleteById with $id")
    }

    @PutMapping("/{id}")
    fun saveTodo(@PathVariable id: Long, @RequestBody todo: TodoEditDto) = service.editTodo(id, todo).also {
        log.info("simulated put request saveTodo with $id and response :{}", it)
    }

    @GetMapping
    fun getAllTodos() = service.getAllTodos()

    @PostMapping("/{id}/done/{isDone}")
    fun markAsDone(@PathVariable id: Long, @PathVariable isDone: Boolean) = service.markAsDone(id, isDone)
}