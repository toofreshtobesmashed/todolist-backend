package sk.murin.todolist.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.crypto.password.NoOpPasswordEncoder
 import org.springframework.security.web.SecurityFilterChain
import org.springframework.web.cors.CorsConfiguration

@Configuration
@EnableWebSecurity
class SecurityConfig() {
    companion object {
        private val CORS_CONFIG = CorsConfiguration().apply {
            allowedHeaders = listOf("Authorization", "Cache-Control", "Content-Type")
            allowedOriginPatterns = listOf("*")
            allowedMethods = listOf("GET", "POST", "DELETE", "PUT")
            allowCredentials = true
            exposedHeaders = listOf("Authorization")
        }

        @Bean
        fun httpConfig(http: HttpSecurity): SecurityFilterChain =
            http.cors().configurationSource { CORS_CONFIG }.and()
                .csrf().disable().authorizeHttpRequests().anyRequest().permitAll().and().build()
    }


    @Bean
    @SuppressWarnings("deprecation")
    fun passwordEncoder(): NoOpPasswordEncoder? {
        return NoOpPasswordEncoder.getInstance() as NoOpPasswordEncoder
    }

}


