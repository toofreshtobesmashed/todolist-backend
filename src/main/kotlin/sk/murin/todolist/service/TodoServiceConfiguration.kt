package sk.murin.todolist.service

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import sk.murin.todolist.repository.TodoRepository

@Configuration
class TodoServiceConfiguration {

    @Bean
    fun todoService(repository: TodoRepository) = TodoService(repo = repository)
}