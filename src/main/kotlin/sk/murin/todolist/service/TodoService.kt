package sk.murin.todolist.service

import sk.murin.todolist.model.Todo
import sk.murin.todolist.model.TodoEditDto
import sk.murin.todolist.repository.TodoEntity
import sk.murin.todolist.repository.TodoRepository

class TodoService(val repo: TodoRepository) {

    fun deleteById(id: Long) = repo.deleteById(id)

    fun editTodo(id: Long, todo: TodoEditDto): Todo {
        val oldTodo = repo.findById(id).get()
        return repo.save(oldTodo.copy(text = todo.text)).toDomain()
    }

    fun getAllTodos(): List<Todo> = repo.findAll().map { it.toDomain() }

    fun addTodo(todo: Todo): Todo {
        val todoEntity = todo.toEntity()
        return repo.save(todoEntity).toDomain()
    }

    fun markAsDone(id: Long, isDone: Boolean) {
        val oldTodo = repo.findById(id).get()
        repo.save(oldTodo.copy(isDone = isDone))
    }

}

fun Todo.toEntity(): TodoEntity = TodoEntity(id = id, modifiedBy = "tbd", text = text)

fun TodoEntity.toDomain(): Todo = Todo(id = id, text = text, isDone = isDone)


