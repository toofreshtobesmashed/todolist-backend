package sk.murin.todolist.repository

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.ZonedDateTime

@Entity
@Table(name = "todo")
data class TodoEntity(
    @Id @GeneratedValue val id: Long = 0L,
    val createdAt: ZonedDateTime = ZonedDateTime.now().plusHours(1),
    val modifiedBy: String = "",
    val isDone: Boolean = false,
    var text: String = "",
)