package sk.murin.todolist.model


data class Todo(
    val id: Long,
    val text: String,
    val isDone: Boolean,
)

data class TodoEditDto(
    val text: String,
)

